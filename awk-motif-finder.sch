#!/usr/bin/awk -f
BEGIN{IGNORECASE=1; FS = ">"; NF > 0 ; 
	print"AWK_motif_finder";
	print "type the DNA sequence to find"; 
	getline inputname < "-" #tambien valdría /dev/stdin
	print "\nseq\tlength\t\tRestriction_site"
}
NF>0{	a=$2; #Como vienen en un formato rarisimo, con espacios y tal, lo pongo así para 
	b=$1; #comprender mejor lo que estoy haciendo. Probablemente innecesario, pero me ayuda
	printf a;
	if (length(b)>0)
		{
		printf "\t"length(b)"\t\t"; totales++; suma=suma+length(b);
			if (posicion=match($0, inputname))
				{
				print posicion;
				 encontrados++
				}
			else
				print "";
		}
}
END{	
	print "\n #STATS# \n";
	print "Total number of sequences: " totales;
	print "Average size of DNA sequences: " suma/totales; #revisar
	print "Number of sequences with " toupper(inputname) " motif: " encontrados;

print "\nDone!\n"
}


