# awk-motif-finder

Un script de awk que encuentra una secuencia dada en un archivo FASTA de DNA

## Descarga
Para usar este método, es necesario tener instalado git en el sistema (`sudo apt install git-all`)
1. Clona este repositorio: `git clone https://codeberg.org/FlyingFlamingo/awk-motif-finder.git`
2. Entra en el directorio: `cd awk-motif-finder`
3. Ejecútalo
